package dev.gondes.flass_flightassistant.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import dev.gondes.flass_flightassistant.R;
import dev.gondes.flass_flightassistant.model.Video;


public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private ArrayList<Video> mVideoData;
    private Context mContext;


    public VideoAdapter(Context context, ArrayList<Video> videoData) {
        this.mVideoData = videoData;
        this.mContext = context;
    }

    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.video_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(VideoAdapter.ViewHolder holder, int position) {
        Video currentVideo = mVideoData.get(position);

        Glide.with(mContext)
                .load("https://img.youtube.com/vi/"+currentVideo.getUrl().substring(32)+"/0.jpg")
                .fitCenter()
                .into(holder.mVideoImage);

        holder.bindTo(currentVideo);
    }

    @Override
    public int getItemCount() {
        return mVideoData.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView mTitleText;
        private ImageView mVideoImage;

        public ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            mTitleText = itemView.findViewById(R.id.title);
            mVideoImage = itemView.findViewById(R.id.videoImage);
        }

        public void bindTo(Video currentVideo) {
            mTitleText.setText(currentVideo.getTitle());
        }

        @Override
        public void onClick(View view) {

            Video currentVideo = mVideoData.get(getAdapterPosition());
            Uri uri = Uri.parse(currentVideo.getUrl());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);

            // Find an activity to hand the intent and start that activity.
            if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                mContext.startActivity(intent);
            } else {
                Log.d("ImplicitIntents", "Can't handle this intent!");
            }

        }
    }
}
