package dev.gondes.flass_flightassistant.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import dev.gondes.flass_flightassistant.fragment.BaggageFragment;
import dev.gondes.flass_flightassistant.fragment.PromoFragment;

public class MainPagerAdapter extends FragmentPagerAdapter {

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return PromoFragment.newInstance("Percobaan", "Percobaan");
            case 1:
                return BaggageFragment.newInstance("Percobaan", "Percobaan");
        }
        return PromoFragment.newInstance("Percobaan", "Percobaan");
    }

    @Override
    public int getCount() {
        return 2;
    }
}
