package dev.gondes.flass_flightassistant.model;

/**
 * Created by hilmi on 15/09/2017.
 */

public class Video {

    private String title;
    private String url;
    private int imageResources;

    public Video(String title, String url, int imageResources) {
        this.title = title;
        this.url = url;
        this.imageResources = imageResources;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public int getImageResources() {
        return imageResources;
    }

}
